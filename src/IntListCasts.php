<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;
use Zlf\Unit\Str;


/**
 * 数组连接转字符串 如 ['a','b']  a,b
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class IntListCasts implements CastsAttributes
{
    /**
     * 获取结果
     * @return array
     */
    public function get($model, $key, $value, $attributes)
    {
        if (is_array($value)) {
            return $value;
        } elseif (is_string($value) && strlen($value) > 0) {
            $data = [];
            foreach (Str::explode(',', $value) as $item) {
                $data[] = (int)$item;
            }
            return $data;
        }
        return [];
    }

    /**
     * 设置数据
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        if (is_array($value)) {
            foreach ($value as $index => $item) {
                $value[$index] = intval($item);
            }
            return implode(',', $value);
        } elseif (is_string($value) && strlen($value) > 0) {
            return $value;
        }
        return '';
    }
}

<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Hyperf\Contract\CastsAttributes;


/**
 * 金额转换，保留2位小数
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class DecimalCasts implements CastsAttributes
{
    /**
     * 获取指定模型和键的结果值
     *
     * @param mixed $model 模型对象，这里未使用，为了保持接口一致性
     * @param string $key 键名，这里未使用，为了保持接口一致性
     * @param mixed $value 要处理的值
     * @param array $attributes 属性数组，这里未使用，为了保持接口一致性
     * @return float 返回浮点数值
     */
    public function get($model, $key, $value, $attributes)
    {
        return floatval($value ? $value : '0');
    }

    /**
     * 设置数据值，并根据需要进行四舍五入
     *
     * @param mixed $model 模型对象，这里未使用，为了保持接口一致性
     * @param string $key 键名，这里未使用，为了保持接口一致性
     * @param mixed $value 要处理的值
     * @param array $attributes 属性数组，这里未使用，为了保持接口一致性
     * @return float|int 如果输入是数字，则返回四舍五入到两位小数的浮点数；否则返回整数0
     */
    public function set($model, $key, $value, $attributes)
    {
        if (empty($value)) {
            return 0;
        }
        if (is_numeric($value)) {
            return round(floatval($value), 2);
        }
        return 0;
    }
}

<?php
declare(strict_types=1);

namespace Zlf\AppHyperfCasts;

use Zlf\Unit\Json;
use Hyperf\Contract\CastsAttributes;


/**
 * json属性设置器
 * Class ArrjoinCasts
 * @package Core\Casts
 */
class JsonCasts implements CastsAttributes
{
    /**
     * 获取结果
     */
    public function get($model, $key, $value, $attributes): array
    {
        if (empty($value)) {
            return [];
        }
        if (is_array($value)) {
            return $value;
        } elseif (is_string($value) && strlen($value) > 0) {
            return Json::decode($value);
        }
        return [];
    }

    /**
     * 设置数据
     */
    public function set($model, $key, $value, $attributes): string
    {
        if (empty($value)) {
            return Json::encode([]);
        }
        if (is_string($value) && strlen($value) > 0) {
            return $value;
        } else if (is_array($value)) {
            return Json::encode($value);
        }
        return Json::encode([]);
    }
}
